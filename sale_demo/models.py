from django.db import models
from django.conf import settings

# Create your models here.


class Game(models.Model):
    CATEGORY = (
        ('Futbol', 'Futbol'),
        ('Dövüş', 'Dövüş'),
        ('Basketbol', 'Basketbol'),
        ('Platform', 'Platform'),
        ('Planet', 'Planet'),
    )

    name = models.CharField('Ad', max_length=50)
    published_year = models.CharField('Yıl', max_length=50)
    category = models.CharField('Kategori', max_length=50, choices=CATEGORY)

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField('Ad Soyad', max_length=50)
    email = models.EmailField('Email', max_length=50)

    def __str__(self):
        return self.name


class Order(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Out for delivery', 'Out for delivery'),
        ('Delivered', 'Delivered'),
    )
    customer = models.ForeignKey(
        Customer, null=True, on_delete=models.SET_NULL)
    game = models.ForeignKey(Game, null=True, on_delete=models.SET_NULL)
    date_created = models.DateTimeField(
        'sipariş tarihi', auto_now_add=True, null=True)
    status = models.CharField('durum', max_length=200,
                              null=True, choices=STATUS)
    

