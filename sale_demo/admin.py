from django.contrib import admin
from sale_demo.models import Game, Customer, Order

# Register your models here.

@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    pass
@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass
@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['customer', 'game', 'status']

