from rest_framework import serializers
from sale_demo.models import Game, Order, Customer


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = '__all__'


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    game_object = GameSerializer(source='game', read_only=True)
    customer_object = CustomerSerializer(source='customer', read_only=True)

    class Meta:
        model = Order
        fields = '__all__'
