from rest_framework import viewsets
from rest_framework import filters, status
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.views import APIView
from sale_demo.models import Game, Order, Customer
from sale_demo.serlializers import OrderSerializer, CustomerSerializer, GameSerializer


# Create your views here.


class GameViewSet(viewsets.ModelViewSet):
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    queryset = Game.objects.all()
    search_fields = ['name', 'published_year', 'category']
    serializer_class = GameSerializer


class CustomerViewSet(viewsets.ModelViewSet):
    serializer_class = CustomerSerializer

    def get_queryset(self):
        queryset = Customer.objects.all()
        username = self.request.query_params.get('username')
        userid = self.request.query_params.get('userid')
        if username is not None:
            queryset = queryset.filter(name__contains=username)
        elif userid is not None:
            queryset = queryset.filter(id=userid)
        return queryset

class UserOrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer

    def get_queryset(self):
        queryset = Order.objects.all()
        user_id = self.request.query_params.get('userid')
        if user_id is not None:
            queryset = queryset.filter(customer=user_id).filter(status="Pending")

            return queryset

        return queryset

class CheckoutViewSet(APIView):

    def get_queryset(self):
        return Order.objects.all()

    def post(self, request, **kwargs):
        user_id = kwargs.get('user_id')
        orders = Order.objects.filter(customer__id=user_id, status="Pending").update(
            status='Out for delivery')

        return Response({'Status': status.HTTP_200_OK,
                         'Affected': orders,
                        'Order Status': 'Out for delivery',
                         'Customer': user_id
                         })
