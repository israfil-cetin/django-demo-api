from django.db import models

from demo_api.managers import SoftDeletionManager
from django.utils import timezone

# Create your models here.


class VehicleModel(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('name', max_length=50, unique=True)
    brand = models.CharField('brand', max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'vehicle_model'
        unique_together = [['name', 'brand']]


class Vehicle(models.Model):
    id = models.AutoField(primary_key=True)
    model = models.ForeignKey(
        VehicleModel, on_delete=models.CASCADE, db_column='vehicle_model_id')
    km = models.IntegerField('km', blank=False)
    plate_num = models.CharField('plaka', max_length=50, blank=False)
    chassis_num = models.CharField('şase numarası', max_length=50, blank=False)
    color = models.CharField('renk', max_length=20, blank=False)
    created_at = models.DateTimeField('created_on', auto_now_add=True)
    updated_at = models.DateTimeField('modified_on', auto_now=True)
    is_deleted = models.DateTimeField(
        'is_deleted', null=True, default=None, blank=True)

    def delete(self):
        self.is_deleted = timezone.now()
        self.save()

    def restore(self):
        self.is_deleted = None
        self.save()

    def __str__(self):
        return self.plate_num

    class Meta:
        db_table = 'vehicle'

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)
