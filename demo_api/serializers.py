from rest_framework import serializers
from demo_api.models import Vehicle, VehicleModel


class VehicleModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = VehicleModel
        fields = '__all__'


class VehicleSerializer(serializers.ModelSerializer):
    model_object = VehicleModelSerializer(source='model', read_only=True)

    class Meta:
        model = Vehicle
        fields = '__all__'
