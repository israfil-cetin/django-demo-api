from django.conf import settings
from django.core.cache import cache
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from demo_api.models import Vehicle, VehicleModel


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


@receiver(post_save, sender=Vehicle)
def vehicle_updated(sender, instance=None, created=False, **kwargs):
    if not created:
        if instance:
            cache.delete(f'vehicle_{instance.pk}')


@receiver(pre_delete, sender=VehicleModel)
def vehicle_model_pre_delete(sender, instance=None, **kwargs):
    if instance:
        for vehicle in instance.vehicle_set.all():
            cache.delete(f'vehicle_{vehicle.pk}')


@receiver(post_save, sender=VehicleModel)
def vehicle_model_updated(sender, instance=None, created=False, **kwargs):
    if not created:
        if instance:
            for vehicle in instance.vehicle_set.all():
                cache.delete(f'vehicle_{vehicle.pk}')
