from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from demo_api.models import Vehicle, VehicleModel
from rest_framework.response import Response
from unittest import TestCase

# Create your tests here.


class VehicleViewSetTestCase(TestCase):
    databases = {'test_db'}
    def setUp(self):
        self.client = APIClient()
        self.vehicle_model = VehicleModel.objects.create(
            name='F40', brand='ferrrai')
        self.vehicle = Vehicle.objects.create(
            km='22', plate_num='asddd', chassis_num='dasddd', color='ddass', model=self.vehicle_model)

    def tearDown(self):
        self.vehicle.delete()
        self.vehicle_model.delete()
        return super().tearDown()

    def test_not_exit_end_point(self):

        response: Response = self.client.get(
            '/fake-endpoint/', content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_vehicle_list(self):

        response: Response = self.client.get(
            '/vehicles/', content_type='application/json', format='json')

        data = response.json()
        self.assertListEqual([*data[0].keys()], ['id', 'model_object', 'km', 'plate_num',
                             'chassis_num', 'color', 'created_at', 'updated_at', 'is_deleted', 'model'])

    def test(self):
        response: Response = self.client.get(
            f'/vehicles/{self.vehicle.pk}/', content_type='application/json', format='json')
        data = response.json()

        self.assertListEqual([*data.keys()], ['id', 'model_object', 'km', 'plate_num',
                             'chassis_num', 'color', 'created_at', 'updated_at', 'is_deleted', 'model'])

        # TODO: Values should be test.


class VehicleModelTestCase(APITestCase):
    pass
    # TODO: Class should be test.
