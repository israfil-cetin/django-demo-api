from django.apps import AppConfig


class DemoApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'demo_api'

    def ready(self):
        import demo_api.handlers
