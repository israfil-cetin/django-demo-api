import pickle

from django.core.cache import cache
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics

from demo_api.models import Vehicle, VehicleModel
from demo_api.serializers import VehicleModelSerializer, VehicleSerializer

# Create your views here.


class VehicleViewSet(viewsets.ModelViewSet):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer

    @method_decorator(never_cache)
    def retrieve(self, request, pk=None, *args, **kwargs):

        serializer_data = cache.get(f'vehicle_{pk}')

        if serializer_data is None:
            instance = self.get_object()
            serializer = self.get_serializer(instance)
            cache.set(f'vehicle_{instance.pk}', serializer.data)

            return Response(serializer.data)

        return Response(serializer_data)


class VehicleModelViewSet(viewsets.ModelViewSet):
    queryset = VehicleModel.objects.all()
    serializer_class = VehicleModelSerializer


class BlackVehicleViewSet(generics.ListAPIView):
    serializer_class = VehicleSerializer
    queryset = Vehicle.objects.filter(color="red")

