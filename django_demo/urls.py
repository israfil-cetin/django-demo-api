"""django_demo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework.authtoken import views as authtoken_views
from demo_api.views import VehicleViewSet, VehicleModelViewSet, BlackVehicleViewSet
from sale_demo.views import GameViewSet, CustomerViewSet, UserOrderViewSet, CheckoutViewSet

router = routers.DefaultRouter()
router.register('vehicles', viewset=VehicleViewSet)
router.register('vehicle-models', viewset=VehicleModelViewSet)
router.register('games', viewset=GameViewSet)
router.register('customers', viewset=CustomerViewSet, basename='customer-detail')
router.register('orders', viewset=UserOrderViewSet, basename='user-order-detail')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api/black-vehicles/', BlackVehicleViewSet.as_view()),
    path('orders/checkout/<int:user_id>', CheckoutViewSet.as_view()),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api-token-auth/', authtoken_views.obtain_auth_token),
    path('accounts/', include('django.contrib.auth.urls')),

]

admin.autodiscover()
admin.site.enable_nav_sidebar = False
