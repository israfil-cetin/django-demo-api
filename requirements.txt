Django==3.2.4
psycopg2-binary==2.9.1
django-filter==2.4.0
djangorestframework==3.12.4
Markdown==3.3.4