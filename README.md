# Django Rest PostgresSQL DB Demo

The project is an example of Django REST Framework with PostgreSQL.

[![Try in PWD](https://raw.githubusercontent.com/play-with-docker/stacks/master/assets/images/button.png)](https://play-with-docker.com/?stack=https://gitlab.com/israfil-cetin/django-demo-api/-/raw/master/docker-compose.yml)

## DataBase Tables
### Vehicle
- id [Primer]
- model [Foreign] (db_column='vehicle_model_id') 
- km
- plate_num
- chassis_num
- color
- created_at
- updated_at
- is_deleted

### VehicleModel
- id [Primer]
- name
- brand

### Game
- CATEGORY (Futbol,Dövüş,Basketbol,Platform,Planet)
- id [Primer]
- name
- published_year
- category
### VehicleModel
- id [Primer]
- name
- email
### VehicleModel
- id [Primer]
- customer [Foreigner]
- game [Foreigner]
- date_created
- status

## How to set up a Python development environment

```bash
python3 -m venv venv

#Linux & Mac: 
source venv/bin/activate

#Windows: 
venv\Scripts\activate
```


## Installation of Requirements 

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
git clone https://gitlab.com/israfil-cetin/django-demo-api

cd django-demo-api

pip install -r requirements.txt

python manage.py makemigrations 

python manage.py migrate

python manage.py createsuperuser
```
## Test

```bash
python manage.py test
```
![](docs/images/test_run.png)

## Postman Collection

[demo-api.postman_collection.json](https://gitlab.com/israfil-cetin/django-demo-api/-/blob/master/docs/demo-api.postman_collection.json)

![](docs/images/postman_image.png)
![](docs/images/postman_image2.png)

## Running the Application



```python
python manage.py runserver
```
![](docs/images/root.png)
## Gitlab Repo
[https://gitlab.com/israfil-cetin/django-demo-api](https://gitlab.com/israfil-cetin/django-demo-api)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GPLv3](https://gitlab.com/israfil-cetin/django-demo-api/-/raw/master/LICENSE/)


